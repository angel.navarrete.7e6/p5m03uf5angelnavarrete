﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P5M03UF5AngelNavarrete
{
    abstract class FiguraGeometrica : IOrdenable
    {
        //Parametres
        protected int Codi { get; set; }
        protected string Nom { get; set; }
        protected ConsoleColor Color { get; set; }

        //Constructors
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, ConsoleColor color)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
        }
        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color;
        }
        public abstract double Area();

        public int Comparar(IOrdenable x)
        {
            FiguraGeometrica figura = (FiguraGeometrica)x;
            if (this.Area() == figura.Area()) return 0;
            else if (this.Area() <= figura.Area()) return 1;
            else return -1;
        }
        public static void Ordenar(IOrdenable[] obj)
        {
            for (int x = 0; x < obj.Length; x++)
            {
                for (int y = 0; y < obj.Length - 1; y++)
                {
                    if (obj[y].Comparar(obj[y + 1]) > obj[y + 1].Comparar(obj[y]))
                    {
                        FiguraGeometrica temporal = (FiguraGeometrica)obj[y];
                        obj[y] = obj[y + 1];
                        obj[y + 1] = temporal;
                    }
                }
            }
        }
    }
    class Rectangle : FiguraGeometrica, IOrdenable
    {
        //Parametres
        private double BaseRectangle { get; set; }
        private double Altura { get; set; }

        //Constructors
        public Rectangle() { }

        public Rectangle(int codi, string nom, ConsoleColor color, double baseRectangle, double altura)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.BaseRectangle = baseRectangle;
            this.Altura = altura;
        }
        public Rectangle(Rectangle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.BaseRectangle = figura.BaseRectangle;
            this.Altura = figura.Altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ", base " + BaseRectangle + ", altura " + Altura;
        }
        public double Perimetre()
        {
            return (2 * BaseRectangle) + (2 * Altura);
        }

        public override double Area()
        {
            return BaseRectangle * Altura;
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Rectangle p = (Rectangle)obj;
            if (this.Codi == p.Codi)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    class Triangle : FiguraGeometrica, IOrdenable
    {
        //Parametres
        private double BaseTriangle { get; set; }
        private double Altura { get; set; }

        //Constructors
        public Triangle() { }

        public Triangle(int codi, string nom, ConsoleColor color, double baseTriangle, double altura)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.BaseTriangle = baseTriangle;
            this.Altura = altura;
        }
        public Triangle(Triangle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.BaseTriangle = figura.BaseTriangle;
            this.Altura = figura.Altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ", base " + BaseTriangle + ", altura " + Altura;
        }
        public double Perimetre()
        {
            return BaseTriangle * 3;
        }
        public override double Area()
        {
            return (BaseTriangle * Altura) / 2;
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Triangle p = (Triangle)obj;
            return (this.Codi == p.Codi);

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    class Cercle : FiguraGeometrica, IOrdenable
    {
        //Parametres
        private double Radi { get; set; }

        //Constructors
        public Cercle() { }

        public Cercle(int codi, string nom, ConsoleColor color, double radi)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.Radi = radi;
        }
        public Cercle(Cercle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.Radi = figura.Radi;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ",radi " + Radi;
        }
        public double Perimetre()
        {
            return 2 * Math.PI * Radi;
        }
        public override double Area()
        {
            return Math.PI * Math.Pow(Radi, 2);
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Cercle p = (Cercle)obj;
            return (this.Codi == p.Codi);

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
