﻿using System;
using System.Linq;

namespace P5M03UF5AngelNavarrete
{
    public interface IOrdenable
    {
        int Comparar(IOrdenable x);
        /* Retorna: 0 si this = x
        <0 si this < x
        >0 si this > x
        */
    }
    public class TaulaOrdenable<T> : IOrdenable
    {
        public T[] Tabla { get; set; }
        public int numElements = 0;
        public TaulaOrdenable() { }
        public TaulaOrdenable(T[] taulaEspecifica)
        {
            this.Tabla = taulaEspecifica;
        }
        public int Capacitat()
        {
            return Tabla.Length;
        }
        
        public int NrElements()
        {
            return numElements;
        }
        public int Afegir(T obj)
        {
            if (obj == null) return -1;
            else if (numElements == Tabla.Length) return -2;
            else
            {
                this.Tabla[numElements] = obj;
                numElements++;
                return 0;
            }
            
        }

        public T ExemplarAt(int posicioUsuari)
        {
            if (posicioUsuari < Tabla.Length-1)
            { 
                return Tabla[posicioUsuari];
            }
            else {
                Console.WriteLine("Error: Posicio fora del Array");
                return default(T);
            }
        }

        public string ExtreureAt(int posicioUsuari) {
            if (posicioUsuari < Tabla.Length - 1)
            {
                string obj = Tabla[posicioUsuari].ToString();
                Tabla[posicioUsuari] = default(T);
                numElements--;
                return obj;
            }
            else
            {

                return null;
            }
        }
        public void Buidar()
        {
            Tabla = new T[Capacitat()];
            numElements = 0;
        }
        public void Visualitzar()
        {
            Console.WriteLine("Capacitat: "+Capacitat());
            Console.WriteLine("Elements: "+NrElements());
            foreach (var item in Tabla)
            {
                if (item != null)
                {
                    Console.WriteLine(item);
                }
                
            }
        }
        public void Ordenar()
        {
            for (int x = 0; x < this.Tabla.Length; x++)
            {
                for (int y = 0; y < this.Tabla.Length - 1; y++)
                {
                    if (this.Tabla[y] != null && this.Tabla[y+1] != null)
                    {
                        IOrdenable a = (IOrdenable)this.Tabla[y];
                        IOrdenable b = (IOrdenable)this.Tabla[y + 1];
                        if (a.Comparar(b) > b.Comparar(a))
                        {
                            T temporal = this.Tabla[y];
                            this.Tabla[y] = this.Tabla[y + 1];
                            this.Tabla[y + 1] = temporal;
                        }
                    }
                }
            }
        }

        public int Comparar(IOrdenable x)
        {
            TaulaOrdenable<T> generic = (TaulaOrdenable<T>)x;
            if (this.numElements == generic.numElements) return 0;
            else if (this.numElements > generic.numElements) return 1;
            else return -1;
        }
    }

    public class TaulaOrdenableFiguraGeometrica<FiguraGeometrica> : TaulaOrdenable <FiguraGeometrica>
    { 
        public TaulaOrdenableFiguraGeometrica(int capacitat)
        {
            if (capacitat < 0)
            {
                this.Tabla = new FiguraGeometrica[10];
            }
            else
            {
                this.Tabla = new FiguraGeometrica[capacitat];
            }
        }
    }

    class ProvaTaulaOrdenableFiguraGeometrica { 
        public static void Main()
        {
            TaulaOrdenableFiguraGeometrica<FiguraGeometrica> taula1 = new TaulaOrdenableFiguraGeometrica<FiguraGeometrica>(50);
            //Creacio Rectangle
            Rectangle rectangle = new Rectangle(2, "Rectasaurio", ConsoleColor.Red, 21, 43);
            //Creacio Cercles
            Cercle cercle1 = new Cercle(8, "ELCIRCULO", ConsoleColor.Blue, 88);
            Cercle cercle2 = new Cercle(7, "ELNOCIRCULO", ConsoleColor.Cyan, 13);
            //Creacio Triangles
            Triangle triangle1 = new Triangle(4, "Obamid", ConsoleColor.Black, 22, 69);
            Triangle triangle2 = new Triangle(4, "Pentagono", ConsoleColor.Red, 30, 90);
            //Afegim les figures
            taula1.Afegir(rectangle);
            taula1.Afegir(cercle1);
            taula1.Afegir(cercle2);
            taula1.Afegir(triangle1);
            taula1.Afegir(triangle2);
            Console.WriteLine("\nTabla antes del Ordenar: ");
            taula1.Visualitzar();
            taula1.Ordenar();
            Console.WriteLine("\nTabla despues del Ordenar: ");
            taula1.Visualitzar();
            //Proves ExemplarAt()
            Console.WriteLine("\nPROVES ExemplarAt()");
            Console.WriteLine(taula1.ExemplarAt(2));
            Console.WriteLine(taula1.ExemplarAt(60));
            //Proves ExtreureAt()
            Console.WriteLine("\nPROVES ExtreureAt()");
            Console.WriteLine(taula1.ExtreureAt(1));
            Console.WriteLine(taula1.ExemplarAt(80));
            //Proves Buidar()
            Console.WriteLine("\nBuidem la Taula: ");
            taula1.Buidar();
            taula1.Visualitzar();
        }
    }
}
